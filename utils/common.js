import moment from "moment";
export function formatDate(date) {
  return moment(date).format("YYYY/MM/DD");
}
export function formatTime(date) {
  return moment(date).format("HH:MM");
}

export function scrollToSmoothly(pos, time) {
  if (isNaN(pos)) {
    throw "Position must be a number";
  }
  if (pos < 0) {
    throw "Position can not be negative";
  }
  var currentPos = window.scrollY || window.screenTop;
  window.scrollTo(0, pos, 'smooth');
}


export function getLastModifiedHours(createDate) {
  if (!createDate) return '05m00'
  else {
    //const hours = moment().diff(moment(createDate));
    var startTime = moment(createDate);
    var endTime = moment();

    // calculate total duration
    var duration = moment.duration(endTime.diff(startTime));

    // duration in hours
    var hours = parseInt(duration.asHours());

    // duration in minutes
    var minutes = parseInt(duration.asMinutes()) % 60;

    return `${hours}H${minutes}m`
  }
}
