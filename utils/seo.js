//import { BACKOFFICE_URL, URL } from "@/services/constant";
export function getMetaTags(seo) {
  if (!seo) return [];
  const tags = [];
  if (seo.og_locale) {
    tags.push({
        property: "og:locale",
        content: seo.og_locale,
    })
  }
  if (seo.og_type) {
    tags.push({
      property: "og:type",
      content: seo.og_type,
    })
  }
  if (seo.title) {
    tags.push({
      property: "og:title",
      content: seo.title,
    })
  }
  if(seo.og_description) {
    tags.push({
      property: "og:description",
      content: seo.og_description,
    })
  }
  if(seo.og_description) {
    tags.push({
      property: "description",
      content: seo.og_description,
    })
  }
  if(seo.og_url) {
    let url = seo.og_url
    if (seo.og_url.indexOf(process.env.BACKOFFICE_URL) != -1) {
      url = url.replace(process.env.BACKOFFICE_URL, process.env.URL)
    }
    tags.push({
      property: "og:url",
      content: url,
    })
  }
  if(seo.og_site_name) {
    tags.push({
      property: "og:site_name",
      content: seo.og_site_name,
    })
  }
  if(seo.article_published_time) {
    tags.push({
      property: "og:published_time",
      content: seo.article_published_time,
    })
  }
  if(seo.article_modified_time) {
    tags.push({
      property: "og:modified_time",
      content: seo.article_modified_time,
    })
  }
  if(seo.og_image && seo.og_image.length > 0) {
    const image = seo.og_image[0]
    if (image.url) {
      tags.push({
        property: "og:image",
        content: image.url,
      })
    }
  }
  if(seo.twitter_card) {
    tags.push({
      property: "twitter:card",
      content: seo.twitter_card,
    })
  }
  if(seo.twitter_misc) {
    let i = 1;
    for(var propName in seo.twitter_misc) {
      tags.push({
        property: `twitter:label${i}`,
        content: seo.twitter_misc[propName],
      })
      i++;
    }
  }
  return tags;
}
