//import * as queries from "../graphql/queries";
//import { API_URL, API_KEY } from "../services/constant";
//const axios = require("axios");
const axios = require("axios");
const https = require('https');

export default async () => {
  const config = {
    method: "get",
    url: ``,
    mode: "no-cors",
    headers: {
      "Content-Type": "application/json",
    },
    httpsAgent: new https.Agent({
      rejectUnauthorized: false
    }),
  };
  let page = 1;
  let per_page = 100;
  config.url = `https://api.demarrez-votre-entreprise.com/wp-json/wp/v2/posts?page=${page}&per_page=${per_page}`;
  const posts = await axios(config).then((response) => {
    return response && response.data ? response.data : null;
  });
  return posts;
};
