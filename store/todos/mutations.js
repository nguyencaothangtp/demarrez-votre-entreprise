const UPDATE_STATUS = (state, value) => {
    state.show = value
}
const UPDATE_TURNON_POPUP = (state, value) => {
  state.isTurnOnPopup = value
}
const UPDATE_FOOTER_BACKGROUND = (state, value) => {
  state.footerHasBackground = value
}
const UPDATE_LOADING = (state, value) => {
  state.loadingPage = value
}
const SHOW_TOP_NOTIFICATION = (state, value) => {
  state.showTopNotification = value
}
const FIRST_LOAD_APP = (state, value) => {
  state.firstLoadApp = value
}
const UPDATE_BOTTOM_POPUP = (state, value) => {
  state.bottomPopup = value
}
export default {
  UPDATE_STATUS, UPDATE_TURNON_POPUP, UPDATE_FOOTER_BACKGROUND, UPDATE_LOADING, SHOW_TOP_NOTIFICATION, FIRST_LOAD_APP, UPDATE_BOTTOM_POPUP
}