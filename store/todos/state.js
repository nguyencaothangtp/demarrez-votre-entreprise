export default () => ({
  show: false, 
  isTurnOnPopup: false,
  footerHasBackground: false,
  loadingPage: false,
  showTopNotification: false,
  firstLoadApp: null,
  bottomPopup: null
})