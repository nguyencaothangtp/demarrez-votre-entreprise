import state from './state';
import getters from './getters';
import mutations from './mutations';



const UPDATE_STATUS = (context, payload) => {
  context.commit('todos/UPDATE_STATUS', payload, {root: true});
}
const UPDATE_TURNON_POPUP = (context, payload) => {
  context.commit('todos/UPDATE_TURNON_POPUP', payload, {root: true});
}
const UPDATE_FOOTER_BACKGROUND = (context, payload) => {
  context.commit('todos/UPDATE_FOOTER_BACKGROUND', payload, {root: true});
}
const UPDATE_LOADING = (context, payload) => {
  context.commit('todos/UPDATE_LOADING', payload, {root: true});
}
const SHOW_TOP_NOTIFICATION = (context, payload) => {
  context.commit('todos/SHOW_TOP_NOTIFICATION', payload, {root: true});
}
const FIRST_LOAD_APP = (context, payload) => {
  context.commit('todos/FIRST_LOAD_APP', payload, {root: true});
}
const UPDATE_BOTTOM_POPUP = (context, payload) => {
  context.commit('todos/UPDATE_BOTTOM_POPUP', payload, {root: true});
}
const actions = {
  UPDATE_STATUS, UPDATE_TURNON_POPUP, UPDATE_FOOTER_BACKGROUND, UPDATE_LOADING, SHOW_TOP_NOTIFICATION, FIRST_LOAD_APP, UPDATE_BOTTOM_POPUP
}

export default {
  namespaced: true,
  state,
  mutations,
  getters,
  actions
}