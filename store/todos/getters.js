const getShow = (state) => {
    return state.show
};
const getTurnOnPopup = (state) => {
    return state.isTurnOnPopup
};

const getLoadingPage = (state) => {
    return state.loadingPage
};

const getShowTopNotification = (state) => {
    return state.showTopNotification
};

const getFirstLoadApp = (state) => {
    return state.firstLoadApp
};
export default () => {
    getShow, getTurnOnPopup, getLoadingPage, getShowTopNotification, getFirstLoadApp
}
