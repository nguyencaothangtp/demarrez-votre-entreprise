import { replace } from "lodash";
export function replaceAMP(htmlStr) {
  let tempHtmlResult = htmlStr;
  try{
    if(tempHtmlResult.indexOf("<img") !== -1)
      tempHtmlResult = replace(tempHtmlResult, "<img", "<amp-img width=\"300\" height=\"500\" layout=\"responsive\"")
    if(tempHtmlResult.indexOf("<font") !== -1)
      tempHtmlResult = replace(tempHtmlResult, "<font", "<div")
    if(tempHtmlResult.indexOf("</font") !== -1)
      tempHtmlResult = replace(tempHtmlResult, "</font", "</div")
    const regex1 = /_msthash=\"(.*?)\"/gi 
    const regex2 = /_mstmutation=\"(.*?)\"/gi
    const regex3 = /_msttexthash=\"(.*?)\"/gi
    const regex4 = /_istranslated=\"(.*?)\"/gi
    const regex5 = /_msthidden=\"(.*?)\"/gi
    tempHtmlResult = replace(tempHtmlResult, regex1, "");
    tempHtmlResult = replace(tempHtmlResult, regex2, "");
    tempHtmlResult = replace(tempHtmlResult, regex3, "");
    tempHtmlResult = replace(tempHtmlResult, regex4, "");
    tempHtmlResult = replace(tempHtmlResult, regex5, "");
  }catch(err){

  }
  return tempHtmlResult;
}
export function handleAMP(htmlStr){
    let tempHtml = htmlStr ? htmlStr : "";
    tempHtml = replaceAMP(tempHtml);
    return tempHtml;
}
export default {
    handleAMP
};

