const axios = require("axios");
const https = require('https');
//domiciliationcommerciale.fr 34.147.65.47
export default {
  async getArticles(page, per_page, categoryId, search) {
    let string = `?page=${page}&per_page=${per_page}&_embed=wp:term`
    if (categoryId) {
      string += `&categories=${categoryId}`
    }
    if (search) {
      string += `&search=${search}`
    }
    const config = {
      method: "get",
      url: `https://api.demarrez-votre-entreprise.com/wp-json/wp/v2/posts${string}`,
      mode: "no-cors",
      headers: {
        "Content-Type": "application/json",
      },
      httpsAgent: new https.Agent({
        rejectUnauthorized: false
      }),
    };
    var paging= 0;
    const posts = await axios(config).then((response) => {
      paging = response.headers['x-wp-totalpages'];
      return response && response.data ? response.data : null;
    });
    let post = null;
    if (posts && posts.length > 0) {
      post = posts;
    }
    return {
      post, paging
    };
  },
  async getPageBySlug(slug) {
    const config = {
      method: "get",
      url: `https://api.demarrez-votre-entreprise.com/wp-json/wp/v2/pages?slug=${slug}`,
      mode: "no-cors",
      headers: {
        "Content-Type": "application/json",
      },
      httpsAgent: new https.Agent({
        rejectUnauthorized: false
      }),
    };
    const posts = await axios(config).then((response) => {
      return response && response.data ? response.data : null;
    });
    let post = null;
    if (posts && posts.length > 0) {
      post = posts[0];
    }
    return post;
  },
  async getPostBySlug(slug) {
    const config = {
      method: "get",
      url: `https://api.demarrez-votre-entreprise.com/wp-json/wp/v2/posts?slug=${slug}`,
      mode: "no-cors",
      headers: {
        "Content-Type": "application/json",
      },
      httpsAgent: new https.Agent({
        rejectUnauthorized: false
      }),
    };
    const posts = await axios(config).then((response) => {
      return response && response.data ? response.data : null;
    });
    let post = null;
    if (posts && posts.length > 0) {
      post = posts[0];
    }
    return post;
  }, 
};
