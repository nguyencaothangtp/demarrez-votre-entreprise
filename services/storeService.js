function loadLocalStoreToState() {
  if (process.browser && window){
    window.$nuxt.$store.dispatch("todos/FIRST_LOAD_APP", 0);
  }
}

export default {
  loadLocalStoreToState
};
