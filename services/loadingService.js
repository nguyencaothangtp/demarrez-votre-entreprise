function showLoading() {
    if (process.browser && window)
      window.$nuxt.$store.dispatch("todos/UPDATE_LOADING", true);
};

function hideLoading() {
    if (process.browser && window)
      window.$nuxt.$store.dispatch("todos/UPDATE_LOADING", false);
};

export default {
    showLoading, hideLoading
}