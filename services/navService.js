export default {
    goToHome() {
      if (process.browser && window && window.$nuxt)
        window.$nuxt.$router.push('/');
    },
    goToNotfound() {
      if (process.browser && window && window.$nuxt)
        window.$nuxt.$router.push('/not-found/404');
    },
    goToArticle(_slug){
      if (process.browser && window && window.$nuxt)
        window.$nuxt.$router.push({
          name: 'article-details',
          params: { slug: _slug }
        });
    },
    goToArticleAMP(_slug){
      if (process.browser && window && window.$nuxt)
        window.$nuxt.$router.push({
          name: 'article-details-amp',
          params: { slug: _slug }
        });
    }
};
  