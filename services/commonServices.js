
import moment from "moment";
import { isArray as _isArray, isDate as _isDate } from "lodash";
//import { API_KEY } from "@/services/constant";

const axios = require("axios");
// export function headerConfig() {
//   return {
//     "x-api-key": API_KEY,
//     "Access-Control-Allow-Origin": "*",
//   };
// }

export function isDate(value) {
  if (typeof value == "object") return _isDate(value);
  return (
    value &&
    !this.isNumber(value) &&
    moment(value).creationData().format != undefined
  );
}
export function getDateFormat(isoDate) {
  return new Date(isoDate).toLocaleDateString('en-GB')
}

export function isArray(value) {
  return value && _isArray(value);
}
export function isNumber(value) {
  return (
    value && !isNaN(value) && parseFloat(value.toString()).toString() != "NaN"
  );
}
export function isBoolean(value) {
  return (
    value != undefined &&
    value != null &&
    (value.toString() === "true" || value.toString() === "false")
  );
}


export function isNumberEvent(event) {
  let pass =
    /[4][8-9]{1}/.test(event.charCode) || /[5][0-7]{1}/.test(event.charCode);
  if (!pass) {
    event.preventDefault();
    return false;
  }
}

export function getPhoneNumerText(phoneNumber){
  if(!phoneNumber) return '';
  let result = '';
  for(let i = 0; i < phoneNumber.length; i++){
      result += phoneNumber[i];
      if(i == 1 || i == 3 || i == 5 || i == 7) {
        result += ' ';
      }
  }
  return result;
}

export function delayRequest(func, time){
  setTimeout(()=>{
    if(func) {
      func();
    }
  },time)
}

export function excerpt(value, start,end) {
  return value ? value.substring(start,end) : '';
}

export function getCategories(value) {
  if (value['_embedded'] && value['_embedded']['wp:term'] && value['_embedded']['wp:term'].length) {
    return value['_embedded']['wp:term'][0][0]['name']
  } 
  return 'Catégorie'
}

export function getAvatar(value, width, height) {
  return value.jetpack_featured_media_url.split("?")[0] + `?resize=${width}%2C${height}&ssl=1`;
}
export function getAvatarUrl(value, width, height) {
  return value.split("?")[0] + `?resize=${width}%2C${height}&ssl=1`;
}
 
export default {
  isNumberEvent, isBoolean, isArray, isNumber, isDate,
  getPhoneNumerText,
  delayRequest,excerpt,getAvatar,getAvatarUrl, getCategories, getDateFormat
}
