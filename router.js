import Vue from "vue";
import Router from "vue-router";
import HomePage from "~/pages/index";
import ArticleDetailsPage from "~/pages/articles/_slug";
import PageDetailsPage from "~/pages/pages/_slug";
import NotFoundPage from "~/pages/common/not-found";
import VTooltip from "v-tooltip";
import NuxtJsonld from 'nuxt-jsonld';
// Import component
import Loading from "vue-loading-overlay";
// Import stylesheet
import "vue-loading-overlay/dist/vue-loading.css"; 

// Directive
Vue.directive('click-outside', {
  bind: function (el, binding, vnode) {
    el.clickOutsideEvent = function (event) {
      // here I check that click was outside the el and his childrens
      if (!(el == event.target || el.contains(event.target))) {
      // and if it did, call method provided in attribute value
        vnode.context[binding.expression](event);
      }
    };
    document.body.addEventListener('click', el.clickOutsideEvent)
  },
  unbind: function (el) {
    document.body.removeEventListener('click', el.clickOutsideEvent)
  },
}); 
Vue.$isAMP = false; 
Vue.use(NuxtJsonld); 
Vue.use(Loading);
Vue.use(VTooltip);
Vue.use(Router);
export function createRouter() {
  return new Router({
    mode: "history",
    routes: [
      {
        path: "/",
        name: "home",
        component: HomePage,
      },      
      {
        path: "/:slug",
        name: "article-details",
        component: ArticleDetailsPage,
      },
      {
        path: "/amp/:slug",
        name: "article-details-amp",
        component: ArticleDetailsPage,
      },
      {
        path: "/page/:slug",
        name: "page-detail",
        component: PageDetailsPage,
      },
      {
        path: "/not-found/404",
        name: "not-found",
        component: NotFoundPage,
      },
      { path: "*", redirect: "/not-found/404" }, 
    ],
    scrollBehavior() {
      return { x: 0, y: 0 };
    },
  });
}
