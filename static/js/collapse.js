const wrappers = document.querySelectorAll(".question");
const boxQuestions = document.querySelectorAll(".box-question");
wrappers.forEach(element => {
   const buttonClick = element.querySelector('button');
   buttonClick.addEventListener("click", () => {
        const box = element.querySelector('.box-question');
        if(box.classList.contains('open')){
            boxQuestions.forEach(box => {
                box.classList.remove('open');
            })
        } else {
            boxQuestions.forEach(box => {
                box.classList.remove('open');
            })
            box.classList.add('open')
        }
        
    });
});
