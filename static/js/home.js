//Scroll
function jsScroll(id) {
  var element = document.getElementById(id);
  var linkPosition = document.getElementById("--link-position");
  if (linkPosition) {
    linkPosition.remove();
  }
  if (element) {
    element.scrollIntoView(true);
    var node = document.createElement("span");
    node.id = "--link-position";
    var textnode = document.createTextNode(" ");
    node.appendChild(textnode);
    node.style.position = "absolute";
    node.style.top = "-80px";
    element.appendChild(node);
    node.scrollIntoView(true);
  } else {
    window.location.pathname = "/amp";
  }
}

const links = document.getElementsByClassName("link-amp");
for (let i = 0; i < links.length; i++) {
  links[i].addEventListener("click", (element) => {
    const className = element.target.className;
    const linkId = className.split(" ")[1].trim();
    jsScroll(linkId);
  });
}

// Header
const button = document.getElementById("btn-collapse-menu");
const header = document.querySelector(".header");
button.addEventListener("click", () => {
  const checkClass = button.classList.contains("menu");
  if (checkClass) {
    button.classList.remove("menu");
    button.classList.add("close");
    header.classList.add("open");
  } else {
    button.classList.add("menu");
    button.classList.remove("close");
    header.classList.remove("open");
  }
});

// FAQ
const wrappers = document.querySelectorAll(".question");
const boxQuestions = document.querySelectorAll(".box-question");
wrappers.forEach((element) => {
  const buttonClick = element.querySelector("button");
  buttonClick.addEventListener("click", () => {
    const box = element.querySelector(".box-question");
    if (box.classList.contains("open")) {
      boxQuestions.forEach((box) => {
        box.classList.remove("open");
      });
    } else {
      boxQuestions.forEach((box) => {
        box.classList.remove("open");
      });
      box.classList.add("open");
    }
  });
});
