
const button = document.getElementById("btn-collapse-menu");
const header = document.querySelector(".header");
button.addEventListener("click", () => {
  const checkClass = button.classList.contains("menu");
  if (checkClass) {
    button.classList.remove("menu");
    button.classList.add("close");
    header.classList.add("open");
  } else {
    button.classList.add("menu");
    button.classList.remove("close");
    header.classList.remove("open");
  }
});