import getRoutes from "./utils/getRoutes";
import { URL, BACKOFFICE_URL } from "./services/constant";
const modifyHtml = (html, url) => {
  const ampInvalid = [
    `!important`
  ];
  for (let i = 0; i < ampInvalid.length; i++) {
    while (html.indexOf(ampInvalid[i]) !== -1) {
      html = html.replace(ampInvalid[i], "");
    }
  }
  return html;
};
export default {
  target: 'server',
  generate: {
    fallback: true
  },
  env: {
    apiUrl: process.env.BACKOFFICE_URL || BACKOFFICE_URL,
    url: process.env.URL || URL,
  },
  head: {
    htmlAttrs: {
      lang: "fr",
    },
    title: "Demarrez votre entreprise",
    meta: [
      {
        hid: "description",
        name: "description",
        content: "Demarrez votre entreprise - Tout savoir pour bien commencer"
      },
      {
        hid: "google-site-verification",
        name: "google-site-verification",
        content: "H7rVZg-a0B3gcbH91mCXsOjSprI72nieJOhafg5DlGs"
      },
      { charset: "utf-8" },
      { name: "viewport", content: "width=device-width, initial-scale=1" },
      { name: "format-detection", content: "telephone=no" },
      { name: "format-detection", content: "address=no" },
      { name: "robots", content: "index, follow" },
      { name: "googlebot", content: "index, follow" },
      { name: "google", content: "notranslate" },
      { name: "apple-mobile-web-app-capable", content: "yes" },
      { name: "apple-mobile-web-app-status-bar-style", content: "blue" },
    ],
    link: [
      { rel: "icon", type: "image/x-icon", href: "/favicon.png" },
      {
        rel: "preload",
        as: "style",
        href: "https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,300;0,400;0,500;0,600;1,300;1,400;1,500;1,600;1,700&display=swap",
      },
      {
        rel: "preload",
        as: "style",
        href: "https://fonts.googleapis.com/css2?family=Poppins:ital,wght@0,300;0,400;0,500;0,600;0,700;1,300;1,400;1,500;1,600;1,700&display=swap",
      },
    ],
    script: [
      // {
      //   src: "https://www.googletagmanager.com/gtag/js?id=G-7CG0ZNWY5H",
      //   async: true,
      // },
      // { src: "/js/ga.js" },
    ],
  },
  /*
   ** Global CSS
   */
  css: ["@assets/scss/main.scss"],
  plugins: [
   
  ],
  ngrok: {
    // module options
    authtoken: "243DPG8RlNjPbsGmqT5qMLWPww7_6GU8LkjE7nNVvpRvAzDkL",
  },
  modules: [
    "@nuxtjs/axios",
    "@nuxtjs/markdownit",
    "@nuxtjs/sitemap",
    "@nuxtjs/robots",
    "@nuxtjs/amp",
    "@luxdamore/nuxt-prune-html",
    //'@nuxtjs/ngrok',
  ],
  robots: {
    Sitemap: "https://demarrez-votre-entreprise.com/sitemap.xml",
    UserAgent: "*",
    Disallow: "/not-found/404",
  },
  buildModules: [
    "@nuxtjs/router",
  ],
  sitemap: {
    hostname: process.env.URL,
    gzip: true,
    routes: async () => {
      const posts = await getRoutes();
      let rts = [];
      for (const article of posts) {
        rts.push({
          url: `/${article.slug}`,
          lastmod: article.modified
        });
      }
      return rts;
    },
    defaults: {
      changefreq: "daily",
      priority: 1,
      lastmod: new Date(),
    },
  },
  markdownit: {
    preset: "default",
    linkify: true,
    breaks: true,
    injected: true,
    html: true,
  },
  amp: {
    css: "~/assets/scss/main.scss",
    //origin: URL || "http://localhost:3000",
    origin: process.env.URL || "http://localhost:3000",
  },
  // Module - default config
  pruneHtml: {
    enabled: true, // `true` in production
    hideGenericMessagesInConsole: false, // `false` in production
    hideErrorsInConsole: false, // deactivate the `console.error` method
    hookRenderRoute: true, // activate `hook:render:route`
    hookGeneratePage: true, // activate `hook:generate:page`
    selectors: [
      // CSS selectors to prune
      'link[rel="preload"][as="script"]',
      'script:not([type="application/ld+json"])',
    ],
    classesSelectorsToKeep: [], // disallow pruning of scripts with this classes, n.b.: each `classesSelectorsToKeep` is appended to every `selectors`, ex.: `link[rel="preload"][as="script"]:not(__classesSelectorsToKeep__)`
    link: [], // inject custom links, only if pruned
    script: [
      {
        src: '/plugins/axeptio.js',
        lazy: true,
        defer: true,
      },
    ], // inject custom scripts, only if pruned
    htmlElementClass: null, // a string added as a class to the <html> element if pruned
    cheerio: {
      // the config passed to the `cheerio.load(__config__)` method
      xmlMode: false,
    },
    types: [
      // it's possibile to add different rules for pruning
      "default-detect",
      // 'query-parameters'
      "headers-exist",
    ],

    // 👇🏻 Type: `default-detect`
    headerNameForDefaultDetection: "user-agent", // The `header-key` base for `MobileDetection`, usage `request.headers[ headerNameForDefaultDetection ]`
    auditUserAgent: "lighthouse", // prune if `request.header[ headerNameForDefaultDetection ]` match, could be a string or an array of strings
    isAudit: true, // remove selectors if match with `auditUserAgent`
    isBot: true, // remove selectors if is a bot
    ignoreBotOrAudit: false, // remove selectors in any case, not depending on Bot or Audit
    matchUserAgent: null, // prune if `request.header[ headerNameForDefaultDetection ]` match, could be a string or an array of strings

    // 👇🏻 Type: 'query-parameters'
    queryParametersToPrune: [
      // array of objects (key-value)
      // trigger the pruning if 'query-parameters' is present in `types` and at least one value is matched, ex. `/?prune=true`
      {
        key: "prune",
        value: "true",
      },
    ],
    queryParametersToExcludePrune: [
      {
        key: "pure",
        value: "false",
      },
    ], // same as `queryParametersToPrune`, exclude the pruning if 'query-parameters' is present in `types` and at least one value is matched, this priority is over than `queryParametersToPrune`

    // 👇🏻 Type: 'headers-exist'
    headersToPrune: [
      {
        key: "pure",
        value: "true",
      },
    ], // same as `queryParametersToPrune`, but it checks `request.headers`
    headersToExcludePrune: [
      {
        key: "pure",
        value: "false",
      },
    ], // same as `queryParamToExcludePrune`, but it checks `request.headers`, this priority is over than `headersToPrune`

    // Emitted events for callbacks methods
    onBeforePrune: null, // ({ result, [ req, res ] }) => {}, `req` and `res` are not available on `nuxt generate`
    onAfterPrune: null, // ({ result, [ req, res ] }) => {}, `req` and `res` are not available on `nuxt generate`
  },
  hooks: {
    // This hook is called before saving the html to flat file
    // 'generate:page': (page) => {
    //   //if (!/^\/amp\//gi.test(page.route)) {
    //   //  page.html = modifyHtml(page.html)
    //   //}
    //   console.log('generate page')
    // },
    // This hook is called before serving the html to the browser
    "render:route": (url, page, { req, res }) => {
      if (/^\/amp\//gi.test(url)) {
        console.log("modified");
        page.html = modifyHtml(page.html, url)
        req.headers["pure"] = "false";
      } else {
        console.log("none modified");
        req.headers["pure"] = "true";
      }
    },
  },
};
